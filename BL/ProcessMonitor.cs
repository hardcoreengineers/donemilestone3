﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BL
{
    public class ProcessMonitor
    {

        public List<proStat> stats { get; set; }
        public float cputotal { get; set; }
        public float memtotal { get; set; }
        public ProcessMonitor()
        {
            Process[] runningNow = Process.GetProcesses();
            stats = new List<proStat>();
            PerformanceCounter cpuCounter;
            cpuCounter = new PerformanceCounter();
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";
            cpuCounter.NextValue();
            cputotal = 0;
            memtotal = 0;

            //start collection: for each process

            foreach (Process process in runningNow)
                try
                {
                    stats.Add(new proStat(process)); //begin monitoring this process's CPU and RAM, cover this line in a try-catch


                }
                catch (Exception e) { }

            //wait... (collection interval)
            Thread.Sleep(1000);
            //end collection: for each process
            foreach (proStat stat in stats)
            {
                stat.Stop(); //run NextValue() on mem and CPU for this process
            }
            PerformanceCounter ramCounter;
            ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            float Avail_ram = ramCounter.NextValue() * 1024 * 1024; //convert to Bytes
            float Tot_ram = (float)(new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory);
            memtotal = (100 * (Tot_ram - Avail_ram) / Tot_ram);
            cputotal = cpuCounter.NextValue();




        }
        public string getCpuTotal()
        {
            float cpuTemp = cputotal * 1000;
            double temp = (int)cpuTemp;

            return temp / 1000 + "%";

        }
        public string getMemoryTotal()
        {
            float memTemp = memtotal * 1000;
            double temp = (int)memTemp;

            return temp / 1000 + "%";

        }
        public void KillProcess(Process p)
        {
            try
            {
                p.Kill();
                Refresh();

            }
            catch (Exception e)
            {

            }
        }
        public void Refresh()
        {
            Process[] runningNow = Process.GetProcesses();
            stats = new List<proStat>();

            //start collection: for each process

            foreach (Process process in runningNow)
                try
                {
                    stats.Add(new proStat(process)); //begin monitoring this process's CPU and RAM, cover this line in a try-catch

                }
                catch (Exception e) { }
            //wait... (collection interval)
            Thread.Sleep(1000);
            //end collection: for each process
            foreach (proStat stat in stats)
            {
                stat.Stop(); //run NextValue() on mem and CPU for this process
            }
        }
    }
}
