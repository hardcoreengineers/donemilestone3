﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharedClasses;
using iTextSharp;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;

namespace BL
{
    class PDFGenerator
    {
        public void GenerateDataLeakagePDF(OrderFile[] files, string directory)
        {
            string fileName = "\\DataLeakageScan";
            string end = ".pdf";
            string total = directory + fileName + end;//Make file total name.
            int j = 0;
            while (File.Exists(total))
            {
                j++;
                total = directory + fileName + j.ToString() + end;
            }
            FileStream fs = new FileStream(total, FileMode.Create, FileAccess.Write, FileShare.None);
            Document DataLeakageDoc = new Document();
            DataLeakageDoc.AddTitle("DataLeakage Documention");
            DataLeakageDoc.AddAuthor("Cyberus Inc");
            DataLeakageDoc.AddSubject("DataLeakage Documention");
            DataLeakageDoc.AddKeywords("This is the dataleakage documention by Cyberus Inc");
            DataLeakageDoc.AddCreationDate();
            PdfWriter writer = PdfWriter.GetInstance(DataLeakageDoc, fs);
            DataLeakageDoc.Open();//Open DataLeakage Doc.
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 28, Font.UNDERLINE, BaseColor.BLACK);
            DataLeakageDoc.Add(new Paragraph("The Data Leakage Scan is : ", times));
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell = new PdfPCell();
            cell.Colspan = 2;
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);
            table.AddCell("File Name :");
            table.AddCell("File Score :");
            for (int i = 0; i < files.Length; i++)//Adding all the files name and score.
            {
                table.AddCell(files[i].getName());
                table.AddCell(files[i].getScore().ToString());
            }
            table.SpacingBefore = 20;          
            DataLeakageDoc.Add(table);
            DataLeakageDoc.Close();
        }

        public void GenerateLogsPDF(List<Log> logs, string directory)
        {
            string fileName = "\\LogsScan";
            string end = ".pdf";
            string total = directory + fileName + end; //Make file total name.
            int j = 0;
            while (File.Exists(total))
            {
                j++;
                total = directory + fileName + j.ToString() + end;
            }
            FileStream fs = new FileStream(total, FileMode.Create, FileAccess.Write, FileShare.None);
            Document LogsDoc = new Document();
            LogsDoc.AddTitle("Logs Documention");
            LogsDoc.AddAuthor("Cyberus Inc");
            LogsDoc.AddSubject("Logs Documention");
            LogsDoc.AddKeywords("This is the logs documention by Cyberus Inc");
            LogsDoc.AddCreationDate();
            PdfWriter writer = PdfWriter.GetInstance(LogsDoc, fs);
            LogsDoc.Open();//Open Logs Doc.
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 28, Font.UNDERLINE, BaseColor.BLACK);
            LogsDoc.Add(new Paragraph("The Logs Scan is : ", times));
            PdfPTable table = new PdfPTable(4);
            PdfPCell cell = new PdfPCell();
            cell.Colspan = 4;
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);
            table.AddCell("The User Performed :");
            table.AddCell("The User Affected :");
            table.AddCell("The Action :");
            table.AddCell("Date And Time :");
            foreach (Log log1 in logs)//Adding all the logs.
            {
                table.AddCell(log1.getUserPerformed());
                if(log1.getUserAffected().Equals(""))
                {
                    table.AddCell("none");
                }
                else
                {
                    table.AddCell(log1.getUserAffected());
                }  
                table.AddCell(log1.getAction());
                table.AddCell(log1.getTime());
            }
            table.SpacingBefore = 20;
            LogsDoc.Add(table);
            LogsDoc.Close();
        }
    }
}
