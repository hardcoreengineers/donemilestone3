﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class proStat
    {
        private PerformanceCounter pc_CPU;
        private PerformanceCounter pc_RAM;
        public string ProcessName { get; set; }
        public float cpuUtil { get; set; }
        public float memUsage { get; set; }
        public Process myProcess { get; set; }

        public proStat(Process proc)
        {
            try
            {
                myProcess = proc;
                //make the counters
                string specialProccessName = GetPerformanceCounterProcessName(proc.Id,
                System.Diagnostics.Process.GetProcessById(proc.Id).ProcessName);
                pc_CPU = new PerformanceCounter("Process", "% Processor Time", specialProccessName, true);
                pc_RAM = new PerformanceCounter("Process", "Working Set", specialProccessName, true); //shared working set
                ProcessName = proc.ProcessName; //actual process name
                cpuUtil = -1;
                memUsage = -1;
                //Tick
                pc_CPU.NextValue();
            }
            catch (Exception e)
            {
                Console.WriteLine("blah");
            }
        }
        public void Stop()
        {
            //Tock
            if (pc_CPU == null)
            {

            }
            else
            {

                cpuUtil = pc_CPU.NextValue() / Environment.ProcessorCount;
                if (cpuUtil == null)
                {
                    throw new Exception("process no longer exists");
                }
                memUsage = pc_RAM.NextValue() / 1024; //convert Bytes to kB
            }
        }
        private string GetPerformanceCounterProcessName(int pid, string processName)
        {
            int nameIndex = 1;
            string value = processName;
            string counterName = processName + "#" + nameIndex;
            PerformanceCounter pc = new PerformanceCounter("Process", "ID Process", counterName, true);
            while (true)
            {
                try
                {
                    if (pid == (int)pc.NextValue())
                    {
                        value = counterName;
                        break;
                    }
                    else
                    {
                        nameIndex++;
                        counterName = processName + "#" + nameIndex;
                        pc = new PerformanceCounter("Process", "ID Process", counterName, true);
                    }
                }
                catch (SystemException ex)
                {
                    if (ex.Message == "Instance '" + counterName + "' does not exist in the specified Category.")
                    {
                        break;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return value;
        }
    }
}
