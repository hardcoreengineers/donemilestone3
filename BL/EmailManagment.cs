﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Mail;

using SharedClasses;
using DAL;

namespace BL
{
    class EmailManagment
    {
        IDAL myDL;
        List<User> [] adminProps;
        public EmailManagment(IDAL theDL)
        {
            adminProps = new List<User>[6];
            for(int i=0;i<adminProps.Length;i++)
            {
                adminProps[i] = new List<User>();
            }
            myDL = theDL;
        }
        public void setAdminProps()
        {
            List<User> admins = myDL.getUsersOfRole("admin");
            foreach(User user in admins)
            {
                bool[] props = myDL.getAdminsProps(user);
                for(int i=0;i<props.Length;i++)
                {
                    if(props[i]==true)
                    {
                        adminProps[i].Add(user);
                    }
                }
            }
        }
        public void changeEmail(User theUser)
        {
            myDL.changeEmail(theUser);
        }
        public void setLiveAlert(User user, bool option)
        {
            myDL.setLiveAlert(user, option);
        }
        public void sendEmails(Log log)
        {
            if(log.getAction().Equals("log_on"))
            {
                sendEmail(adminProps[0], log.ToString());
            }
            else if(log.getAction().Equals("log_off"))
            {
                sendEmail(adminProps[1], log.ToString());
            }
            else if (log.getAction().Equals("changed_password"))
            {
                sendEmail(adminProps[2], log.ToString());
            }
            else if (log.getAction().Equals("encryption_tool_opened"))
            {
                sendEmail(adminProps[3], log.ToString());
            }
            else if(log.getAction().Equals("data_leakage_tool_opened"))
            {
                sendEmail(adminProps[4], log.ToString());
            }
            else
            {
                sendEmail(adminProps[5], log.ToString());
            }
        }
        public void sendEmail(List<User> to,string message)
        {
            try
            {
                SmtpClient Smtp_Server = new SmtpClient();
                MailMessage e_mail;
                Smtp_Server.UseDefaultCredentials = false;
                Smtp_Server.Credentials = new NetworkCredential("CyberusCooperation@gmail.com", "israelrules");
                Smtp_Server.Port = 587;
                Smtp_Server.EnableSsl = true;
                Smtp_Server.Host = "smtp.gmail.com";
                e_mail = new MailMessage();
                e_mail.From = new MailAddress("CyberusCooperation@gmail.com");
                foreach(User user in to)
                {
                    if (user.liveAlert)
                    {
                        e_mail.To.Add(user.Email);
                    }
                }
                e_mail.Subject = "New log entry was added";
                e_mail.IsBodyHtml = false;
                e_mail.Body = message;
                Smtp_Server.Send(e_mail);
            }
            catch (Exception error)
            {
            }
        }
    }
}
