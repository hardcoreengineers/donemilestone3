﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for LiveAlerts.xaml
    /// </summary>
    public partial class LiveAlerts : UserControl
    {
        IBL myBL;
        User me;
        bool [] props;
        Button[] buttons;
        public LiveAlerts(IBL theBL,User user)
        {
            myBL = theBL;
            me = user;
            
            InitializeComponent();
            initLiveAlertButton();
        }

        private void initLiveAlertButton()
        {
            initLiveAlertProps();
            if (me.liveAlert)
            {
                LiveAlertsButton.Content = "Live Alerts : On";              
            }
            else
            {
                LiveAlertsButton.Content = "Live Alerts : Off";
            }
        }
        public void initLiveAlertProps()
        {
            buttons = new Button[6];
            buttons[0] = log_on;
            buttons[1] = log_off;
            buttons[2] = changed_password;
            buttons[3] = encryption_tool_opened;
            buttons[4] = data_leakage_tool_opened;
            buttons[5] = process_monitor_opened;

            props = myBL.getAdminsProp(me);
            for(int i=0;i<props.Length;i++)
            {
                if(props[i] == true)
                {
                    buttons[i].Content = buttons[i].Content + " :On";
                }
                else
                {
                    buttons[i].Content = buttons[i].Content + " :Off";
                }
            }

        }
        private void LiveAlerts_Click(object sender, RoutedEventArgs e)
        {
            if(me.liveAlert)
            {
                myBL.setLiveAlert(me,false);
                me.liveAlert = false;
                LiveAlertsButton.Content = "Live Alerts : Off";
                for(int i=0;i<props.Length;i++)
                {
                    props[i] = false;
                    string buttonText = buttons[i].Content.ToString();
                    buttons[i].Content = buttonText.Substring(0, buttonText.IndexOf(':') + 1)+"Off";
                }
                myBL.setAdminsProps(props,me);
            }
            else
            {
                me.liveAlert = true;
                myBL.setLiveAlert(me,true);
                LiveAlertsButton.Content = "Live Alerts : On";
                for (int i = 0; i < props.Length; i++)
                {
                    props[i] = true;
                    string buttonText = buttons[i].Content.ToString();
                    buttons[i].Content = buttonText.Substring(0, buttonText.IndexOf(':') + 1) + "On";
                }
                myBL.setAdminsProps(props,me);
            }
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new Account(myBL, me);
        }

        private void log_on_Click(object sender, RoutedEventArgs e)
        {
            if(props[0] == true)
            {
                log_on.Content = "User Log On :Off";
                props[0] = false;
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] != false)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 5)
                        {
                            me.liveAlert = false;
                            LiveAlertsButton.Content = "Live Alerts: Off";
                            myBL.setLiveAlert(me, false);
                        }
                    }
                }                
                myBL.setAdminsProps(props, me);
            }
            else
            {
                log_on.Content = "User Log On :On";
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] == false)
                    {
                        if (i == 5)
                        {
                            me.liveAlert = true;
                            myBL.setLiveAlert(me, true);
                            LiveAlertsButton.Content = "Live Alerts: On";
                        }
                    }
                }
                props[0] = true;
                myBL.setAdminsProps(props, me);             
            }
        }

        private void log_off_Click(object sender, RoutedEventArgs e)
        {
            if (props[1] == true)
            {
                log_off.Content = "User Log Off :Off";
                props[1] = false;
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] != false)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 5)
                        {
                            me.liveAlert = false;
                            LiveAlertsButton.Content = "Live Alerts: Off";
                            myBL.setLiveAlert(me, false);
                        }
                    }
                }
                myBL.setAdminsProps(props, me);
            }
            else
            {
                log_off.Content = "User Log Off :On";
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] == false)
                    {
                        if (i == 5)
                        {
                            me.liveAlert = true;
                            myBL.setLiveAlert(me, true);
                            LiveAlertsButton.Content = "Live Alerts: On";
                        }
                    }
                }
                props[1] = true;
                myBL.setAdminsProps(props, me);
            }
        }

        private void changed_password_Click(object sender, RoutedEventArgs e)
        {
            if (props[2] == true)
            {
                changed_password.Content = "User Changed Password :Off";
                props[2] = false;
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] != false)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 5)
                        {
                            me.liveAlert = false;
                            LiveAlertsButton.Content = "Live Alerts: Off";
                            myBL.setLiveAlert(me, false);
                        }
                    }
                }
                myBL.setAdminsProps(props, me);
            }
            else
            {
                changed_password.Content = "User Changed Password :On";
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] == false)
                    {
                        if (i == 5)
                        {
                            me.liveAlert = true;
                            myBL.setLiveAlert(me, true);
                            LiveAlertsButton.Content = "Live Alerts: On";
                        }
                    }
                }
                props[2] = true;
                myBL.setAdminsProps(props, me);
            }
        }
        private void encryption_tool_opened_Click(object sender, RoutedEventArgs e)
        {
            if (props[3] == true)
            {
                encryption_tool_opened.Content = "User Opened Encryption Tool :Off";
                props[3] = false;
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] != false)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 5)
                        {
                            me.liveAlert = false;
                            LiveAlertsButton.Content = "Live Alerts: Off";
                            myBL.setLiveAlert(me, false);
                        }
                    }
                }
                myBL.setAdminsProps(props, me);
            }
            else
            {
                encryption_tool_opened.Content = "User Opened Encryption Tool :On";
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] == false)
                    {
                        if (i == 5)
                        {
                            me.liveAlert = true;
                            myBL.setLiveAlert(me, true);
                            LiveAlertsButton.Content = "Live Alerts: On";
                        }
                    }
                }
                props[3] = true;
                myBL.setAdminsProps(props, me);
            }
        }

        private void data_leakage_tool_opened_Click(object sender, RoutedEventArgs e)
        {
            if (props[4] == true)
            {
                data_leakage_tool_opened.Content = "User Opened Data Leakage Tool :Off";
                props[4] = false;
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] != false)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 5)
                        {
                            me.liveAlert = false;
                            LiveAlertsButton.Content = "Live Alerts: Off";
                            myBL.setLiveAlert(me, false);
                        }
                    }
                }
                myBL.setAdminsProps(props, me);
            }
            else
            {
                data_leakage_tool_opened.Content = "User Opened Data Leakage Tool :On";
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] == false)
                    {
                        if (i == 5)
                        {
                            me.liveAlert = true;
                            myBL.setLiveAlert(me, true);
                            LiveAlertsButton.Content = "Live Alerts: On";
                        }
                    }
                }
                props[4] = true;
                myBL.setAdminsProps(props, me);
            }
        }

        private void process_monitor_opened_Click(object sender, RoutedEventArgs e)
        {
            if (props[5] == true)
            {
                process_monitor_opened.Content = "User Opened Process Monitor :Off";
                props[5] = false;
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] != false)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 5)
                        {
                            me.liveAlert = false;
                            LiveAlertsButton.Content = "Live Alerts: Off";
                            myBL.setLiveAlert(me, false);
                        }
                    }
                }
                myBL.setAdminsProps(props, me);
            }
            else
            {
                process_monitor_opened.Content = "User Opened Process Monitor :On";
                int i;
                for (i = 0; i < 6; i++)
                {
                    if (props[i] == false)
                    {
                        if (i == 5)
                        {
                            me.liveAlert = true;
                            myBL.setLiveAlert(me, true);
                            LiveAlertsButton.Content = "Live Alerts: On";
                        }
                    }
                }
                props[5] = true;
                myBL.setAdminsProps(props, me);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(EmailText.Text == null || EmailText.Text=="")
            {
                MessageBox.Show("Insert new Email");
            }
            else
            {
                me.Email = EmailText.Text;
                myBL.changeEmail(me);
                MessageBox.Show("Updated successfully!");
            }
        }
    }
}
