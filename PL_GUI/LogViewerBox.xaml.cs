﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;
namespace PL_GUI
{
    /// <summary>
    /// This is just the viwer of the log file which only accessible by admins
    /// </summary>
    public partial class LogViewerBox : UserControl
    {
        IBL myBL;
        User me;
        List<Log> logList;
        public LogViewerBox(IBL theBL,User user,List<Log> logs)
        {
            me = user;
            InitializeComponent();
            logList = logs;
            myBL = theBL;
            List<string> lst = new List<string>();
            foreach(Log lg in logs)
            {
                lst.Add(lg.ToString());
            }
            logsText.ItemsSource = lst;
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            UsersControl uc= new UsersControl(myBL, me);
            this.Content = uc;
        }

        private void PDFButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            if (result.ToString().Equals("OK"))
            {
                string dir = dialog.SelectedPath.ToString();
                myBL.generateLogPDF(logList,dir);
                MessageBox.Show("The Logs PDF file has been generated successfully");
            }
        }
    }
}
