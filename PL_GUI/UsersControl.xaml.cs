﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;

namespace PL_GUI
{
    /// <summary>
    /// The UsersControl page is basically a viwer that works acorrding to the user role.
    /// Admins can see any user and edit anyone but themselves
    /// Manager can see employees and edit employees only
    /// Only admins can view the log file
    /// When the admin/manager creates a new user he can choose his role accordingly - admins can create any role,managers can create employees and managers
    /// </summary>
    public partial class UsersControl : UserControl
    {
        IBL myBL;
        User me;
        List<User> allEmployees;
        List<User> allManagers;
        List<User> allAdministrators;

        public UsersControl(IBL theBL,User user)
        {
            me = user;
            myBL = theBL;
            allEmployees = myBL.getUsersOfRole("employee");
            allManagers = myBL.getUsersOfRole("manager");
            allAdministrators = myBL.getUsersOfRole("admin");

            InitializeComponent();
            if (me.role.Equals("manager"))
            {
                allEmployeesGrid.ItemsSource = allEmployees;
                ButtonsPanel.Orientation = Orientation.Vertical;
            }
            else
            {
                LogFileButton.Visibility = Visibility.Visible;

                allEmployeesGrid.ItemsSource = allEmployees;

                allManagersGrid.ItemsSource = allManagers;
                allManagersGrid.Visibility = Visibility.Visible;
                ManagersLabel.Visibility = Visibility.Visible;

                allAdminsGrid.ItemsSource = allAdministrators;
                AdminLabel.Visibility = Visibility.Visible;
                allAdminsGrid.Visibility = Visibility.Visible;       
            }

        }
        public void GoBack_Click(object sender, RoutedEventArgs e)
        {
            MainMenu mm = new MainMenu(myBL, me);
            this.Content = mm;
        }
        private void CreateUser_Click(object sender, RoutedEventArgs e)
        {
            CreateUser cu = new CreateUser(myBL, me);
            this.Content = cu;
        }

        private void LogFileButton_Click(object sender, RoutedEventArgs e)
        {
            LogViewerBox lv = new LogViewerBox(myBL,me,myBL.getLog());
            this.Content = lv;
            
        }

        private void EditEmployee_Button_Click(object sender, RoutedEventArgs e)
        {
            EditUser eu = new EditUser(myBL, me, (User)allEmployeesGrid.SelectedItem);
            this.Content = eu;
        }

        private void RemoveManager_Button_Click(object sender, RoutedEventArgs e)
        {
            EditUser eu = new EditUser(myBL, me, (User)allManagersGrid.SelectedItem);
            this.Content = eu;
        }

        private void EditAdmin_Button_Click(object sender, RoutedEventArgs e)
        {
            if (me.getUsername().Equals(((User)allAdminsGrid.SelectedItem).getUsername()))
            {
                MessageBox.Show("You Can't Edit Yourself");
            }
            else
            {
                EditUser eu = new EditUser(myBL, me, (User)allAdminsGrid.SelectedItem);
                this.Content = eu;
            }
        }
    }

}

