﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PL_GUI
{
    /*
     Interface of Presesntation Layer
      */
    public interface IPL
    {
        void Run();
    }
}
