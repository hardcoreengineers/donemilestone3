﻿using BL;
using PL_GUI;
using SharedClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ProcessView : UserControl
    {
        IBL myBL;
        User me;
        List<proStat> stats;
        ProcessMonitor Monitor;
        public ProcessView(IBL myBL, User u)
        {
            this.me = u;
            this.myBL = myBL;
            Monitor = new ProcessMonitor();
            stats = Monitor.stats;
            if (me != null)
            {
                Log log = new Log("process_monitor_opened", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), me.getUsername(), "");
                myBL.addLog(log);
                myBL.sendEmails(log);
            }
            InitializeComponent();
            allProcessGrid.ItemsSource = stats;
            cpuUsage.Content += " " + Monitor.getCpuTotal();
            memoryUsage.Content += " " + Monitor.getMemoryTotal();

        }
        public void GoBack_Click(object sender, RoutedEventArgs e)
        {
            MainMenu mm = new MainMenu(myBL, me);
            this.Content = mm;
        }
        private void KillProcess_Button_Click(object sender, RoutedEventArgs e)
        {
            proStat Removed = (proStat)allProcessGrid.SelectedItem;
            Monitor.KillProcess(Removed.myProcess);
            Monitor = new ProcessMonitor();
            stats = Monitor.stats;
            allProcessGrid.ItemsSource = stats;
            cpuUsage.Content = "Cpu in use: " + Monitor.getCpuTotal();
            memoryUsage.Content = "Memory in use: " + Monitor.getMemoryTotal();
            allProcessGrid.Items.Refresh();
        }
        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            Monitor = new ProcessMonitor();
            stats = Monitor.stats;
            allProcessGrid.ItemsSource = stats;
            cpuUsage.Content = "Cpu in use: " + Monitor.getCpuTotal();
            memoryUsage.Content = "Memory in use: " + Monitor.getMemoryTotal();
            allProcessGrid.Items.Refresh();
        }
    }
}
