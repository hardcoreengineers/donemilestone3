﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using SharedClasses;

namespace PL_GUI
{
    /// <summary>
    /// This is the mainmenu frame
    /// Every user has 4 options: EncrytionTools Account UsersControl and Data-Leakage
    /// if you're an employee you can't choose UsersControl
    /// </summary>
    public partial class MainMenu : UserControl
    {
        IBL myBL;
        User me;
        public MainMenu(IBL theBL, User user)
        {
            myBL = theBL;
            me = user;
            InitializeComponent();
            if (user != null)
            {
                
            }
            else
            {
                Account_Button.Visibility = Visibility.Collapsed;
                UC_Button.Visibility = Visibility.Collapsed;
            }
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            if (me != null)
            {
                MessageBox.Show("Goodbye! , " + me.getUsername());
                Log log_off = new Log("log_off", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), me.getUsername(), "");
                myBL.addLog(log_off);
                myBL.sendEmails(log_off);
            }
            LoginMenu lm = new LoginMenu(myBL);
            this.Content = lm;

        }


        private void Account_Button_Click(object sender, RoutedEventArgs e)
        {
            UserControl uc = new Account(myBL, me);
            this.Content = uc;
            
        }

        private void ET_button_Click(object sender, RoutedEventArgs e)
        {
            EncryptionTools ET = new EncryptionTools(myBL, me);
            this.Content = ET;
        }

        private void DLT_Button_Click(object sender, RoutedEventArgs e)
        {
            DataLeakageTool DLT = new DataLeakageTool(myBL, me);
            this.Content = DLT;
        }

        private void UC_Button_Click(object sender, RoutedEventArgs e)
        {
            if (me.getRole().Equals("employee"))
            {
                MessageBox.Show("You don't have any permissions");

            }
            else
            {
                UsersControl uc = new UsersControl(myBL, me);
                this.Content = uc;
            }
            
        }

        private void ProcessMonitor_Click(object sender, RoutedEventArgs e)
        {
            ProcessView PV = new ProcessView(myBL, me);
            this.Content = PV;
        }
    }
}
