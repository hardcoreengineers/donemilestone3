﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel; // CancelEventArgs

using BL;

namespace PL_GUI
{
    /// <summary>
    /// This is the windows class where all user control classes are being shown
    /// </summary>
    public partial class Program :Window ,IPL
    {
        IBL myBL;
        public Program(IBL myBL)
        {
            this.myBL = myBL;
            InitializeComponent();
        }
        public void Run()
        {
            this.Show();
            UserControl LM = new LoginMenu(myBL);
            this.Content = LM;
         }
    }
    
}
