﻿using SharedClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Testing
{
    class FunctionsTesting
    {
        static void Main(string[] args)
        {           
            int errors = 0;
            //Set up for the testing.
            User us1 = new User("tomer","8785","employee");
            //Test for getUsername function. #1
            try
            {
                Assert.AreEqual(us1.getUsername(), "tomer");
            }
            catch(Exception e)
            {
                Console.WriteLine("getUsername function doesnt work well");
                errors++;
            }
            //Test for getPassword function. #2
            try
            {
                Assert.AreEqual(us1.getPassword(), "8785");
            }
            catch (Exception e)
            {
                Console.WriteLine("getPassword function doesnt work well");
                errors++;
            }
            //Test for getUsername function. #3
            try
            {
                Assert.AreEqual(us1.getRole(), "employee");
            }
            catch (Exception e)
            {
                Console.WriteLine("getRole function doesnt work well");
                errors++;
            }
            //Test for setPassword function. #4
            try
            {
                us1.setPassword("1234");
                Assert.AreEqual(us1.getPassword(), "1234");
            }
            catch (Exception e)
            {
                Console.WriteLine("setPassword function doesnt work well");
                errors++;
            }
            //Test for getUsername function. #5
            try
            {
                us1.setRole("admin");
                Assert.AreEqual(us1.getRole(), "admin");
            }
            catch (Exception e)
            {
                Console.WriteLine("setRole function doesnt work well");
                errors++;
            }
            if(errors==0)
            {
                Console.WriteLine("The test has passed succesfully");
            }
            else
            {
                Console.WriteLine("You have " + errors + " errors in the test");
            }                  
            Console.ReadLine();
        }
    }
}
