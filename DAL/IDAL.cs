﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;

namespace DAL
{
    /*
     Interface of Data Access Layer
      */
    public interface IDAL
    {
        bool verify(string username, string password);
        List<User> getUsersOfRole(string role);
        void changePassword(User user, string newPassword);
        void deleteUser(User user);
        void addUser(User user);
        void changeRole(User user, string newRole);
        bool isExist(string username);
        string getRole(string username);
        void addLog(Log log);
        List<Log> getLogs();
        void changeEmail(User user);
        void setLiveAlert(User user, bool option);
        bool [] getAdminsProps(User user);
        void setAdminsProps(bool [] props,User user);
        User getUser(String username);
        bool isDataConnection();
    }
}
