﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using SharedClasses;

namespace DAL
{
    class UserAccess
    {
        public UserAccess()
        {

        }
        readonly string connectionString= "user id=DESKTOP-TA7DGMV\\kutig;" + "password=;server=DESKTOP-TA7DGMV\\DATABASE_MIL_3;" + "Trusted_Connection=yes;" + "database=Database_Mile3;" + "connection timeout=30;" + "MultipleActiveResultSets = true";
        readonly static string address = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/UserDatabase.txt";
        public bool verify(string username,string password)//Return the user.
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            try
            {
                SqlCommand verifyCommand = new SqlCommand("SELECT * FROM Users WHERE username='"+username+"' AND password='"+password+"'", myConnection);
                SqlDataReader usersReader = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
                if(usersReader.HasRows)
                {
                    myConnection.Close();
                    return true;
                }
                else
                {
                    myConnection.Close();
                    return false;
                }
            }
            catch (Exception e)
            {
                myConnection.Close();
                return false;
            }
        }
        public List<User> getUsersOfRole(string role)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand getCommand = new SqlCommand("SELECT * FROM Users WHERE role='" + role + "'", myConnection);
            SqlDataReader usersReader = getCommand.ExecuteReader(); //exceute query and store response in a serilalized
            List<User> lst = new List<User>();
            while (usersReader.Read()) //while there are more rows to read... move pointer to the next row
            {
                if(role.Equals("admin"))
                {
                    lst.Add(new User(usersReader["username"].ToString(), usersReader["password"].ToString(), usersReader["role"].ToString(), usersReader["email"].ToString(), Convert.ToBoolean(usersReader["liveAlerts"].ToString())));
                }
                else
                {
                    lst.Add(new User(usersReader["username"].ToString(), usersReader["password"].ToString(), usersReader["role"].ToString()));
                }
            }
            myConnection.Close();
            return lst;
        }
        public void changePassword(User user,string newPassword)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand updateCommand = new SqlCommand("UPDATE Users SET password = '"+newPassword+"' WHERE username = '"+user.getUsername()+"'", myConnection);
            updateCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public void deleteUser(User user)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand deleteCommand = new SqlCommand("DELETE FROM Users where username='"+user.getUsername()+"'", myConnection);
            deleteCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public void addUser(User user)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            int selection;
            if(user.liveAlert==true)
            {
                selection = 1;
            }
            else
            {
                selection = 0;
            }

            myConnection.Open();
            SqlCommand insertCommand = new SqlCommand("INSERT INTO Users (username,password,role,email,liveAlerts)" + "VALUES ('" + user.getUsername() + "','" + user.getPassword() + "', '" + user.getRole() +"','"+user.Email+"',"+selection+")", myConnection);
            insertCommand.ExecuteNonQuery();
            if(user.getRole().Equals("admin"))
            {
                insertCommand = new SqlCommand("INSERT INTO AdminsProps (username,log_on,log_off,changed_password,changed_role,removed_user,encryption_tool,data_leakage_tool,process_monitor)" + "VALUES ('" + user.getUsername() + "',0,0,0,0,0,0,0,0 )", myConnection);
                insertCommand.ExecuteNonQuery();
            }
            myConnection.Close();
        }
        public void changeRole(User user,string newRole)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand updateCommand = new SqlCommand("UPDATE Users SET role = '" + newRole + "' WHERE username = '" + user.getUsername() + "'", myConnection);
            updateCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public bool isExist(string username)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand verifyCommand = new SqlCommand("SELECT * FROM Users WHERE username='" + username +"'", myConnection);
            SqlDataReader usersReader = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
            if (usersReader.HasRows)
            {
                myConnection.Close();
                return true;
            }
            else
            {
                myConnection.Close();
                return false;
            }
        }
        public string getRole(string username)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand verifyCommand = new SqlCommand("SELECT * FROM Users WHERE username='" + username + "'", myConnection);
            SqlDataReader usersReader = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
            usersReader.Read();
            String role= usersReader["role"].ToString();
            myConnection.Close();
            return role;
        }
        public void changeUsers(string[] users)//Changes the users.
        {
            File.WriteAllLines(address, users);
        }
        public void changeEmail(User theUser)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand updateCommand = new SqlCommand("UPDATE Users SET email = '" + theUser.Email + "' WHERE username = '" + theUser.getUsername() + "'", myConnection);
            updateCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public User getUser(String username)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand verifyCommand = new SqlCommand("SELECT * FROM Users WHERE username='" + username + "'", myConnection);
            SqlDataReader usersReader1 = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
            usersReader1.Read();
            String role = usersReader1["role"].ToString();
            String pass = usersReader1["password"].ToString();
            String email= usersReader1["email"].ToString();
            bool liveAlerts = Convert.ToBoolean(usersReader1["liveAlerts"].ToString());
            myConnection.Close();
            return new User(username,pass,role,email,liveAlerts);
        }
        public void setLiveAlert(User theUser,bool option)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            int selection;
            myConnection.Open();
            if (option == true)
                selection = 1;
            else
                selection = 0;
            SqlCommand updateCommand = new SqlCommand("UPDATE Users SET liveAlerts = " + selection + " WHERE username = '" + theUser.getUsername() + "'", myConnection);
            updateCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public bool [] getAdminsProps(User user) //Fix
        {
            bool[] props = new bool[6];
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            SqlCommand verifyCommand = new SqlCommand("SELECT * FROM AdminsProps where username='"+user.getUsername()+"'", myConnection);
            SqlDataReader usersReader = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
            usersReader.Read();
            props[0] = Convert.ToBoolean(usersReader["log_on"].ToString());
            props[1] = Convert.ToBoolean(usersReader["log_off"].ToString());
            props[2] = Convert.ToBoolean(usersReader["changed_password"].ToString());
            props[3] = Convert.ToBoolean(usersReader["encryption_tool"].ToString());
            props[4] = Convert.ToBoolean(usersReader["data_leakage_tool"].ToString());
            props[5] = Convert.ToBoolean(usersReader["process_monitor"].ToString());
            myConnection.Close();
            return props;
        }
        public void setAdminsProps(bool [] props,User user) //Fix
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            int log_on = 0, log_off = 0, changed_password = 0, encryption_tool = 0, data_leakage_tool = 0, process_monitor = 0;
            if(props[0])
            {
                log_on = 1;
            }
            if (props[1])
            {
                log_off = 1;
            }
            if (props[2])
            {
                changed_password = 1;
            }
            if (props[3])
            {
                encryption_tool = 1;
            }
            if (props[4])
            {
                data_leakage_tool = 1;
            }
            if (props[5])
            {
                process_monitor = 1;
            }
            SqlCommand updateCommand = new SqlCommand("UPDATE AdminsProps SET log_on = " + log_on + ",log_off="+log_off+",changed_password="+changed_password+",encryption_tool="+encryption_tool+",data_leakage_tool="+data_leakage_tool+",process_monitor="+process_monitor+" WHERE username = '" + user.getUsername() + "'", myConnection);
            updateCommand.ExecuteNonQuery();
            myConnection.Close();
        }
    }
}
