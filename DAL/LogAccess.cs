﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharedClasses;
using System.Data.SqlClient;

namespace DAL
{
    public class LogAccess
    {
        readonly SqlConnection myConnection = new SqlConnection("user id=DESKTOP-TA7DGMV\\kutig;" + "password=;server=DESKTOP-TA7DGMV\\DATABASE_MIL_3;" + "Trusted_Connection=yes;" + "database=Database_Mile3;" + "connection timeout=30");
        readonly static string address = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/LogDatabase.txt";
        public List<Log> getLogs()//Return the all logs.
        {
            myConnection.Open();
            SqlCommand getCommand = new SqlCommand("SELECT * FROM Logs",myConnection);
            SqlDataReader usersReader = getCommand.ExecuteReader(); //exceute query and store response in a serilalized
            List<Log> lst = new List<Log>();
            while (usersReader.Read()) //while there are more rows to read... move pointer to the next row
            {
                lst.Add(new Log(usersReader["action"].ToString(), usersReader["timeOfAction"].ToString(), usersReader["userPerformed"].ToString(), usersReader["userAffected"].ToString()));
            }
            myConnection.Close();
            return lst;
        }
        public void insertLog(Log log)//Changes the logs.
        {
            myConnection.Open();
            SqlCommand insertCommand = new SqlCommand("INSERT INTO Logs (action,timeOfAction,userPerformed,userAffected)" + "VALUES ('" + log.getAction() + "','" + log.getTime() + "', '" + log.getUserPerformed() +"', '"+log.getUserAffected()+ "')", myConnection);
            insertCommand.ExecuteNonQuery();
            myConnection.Close();
        }

    }
}
