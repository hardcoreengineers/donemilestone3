﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharedClasses;
using System.Data.SqlClient;


namespace DAL
{
    public class DataAccess : IDAL
    {
        readonly string connectionString = "user id=DESKTOP-TA7DGMV\\kutig;" + "password=;server=DESKTOP-TA7DGMV\\DATABASE_MIL_3;" + "Trusted_Connection=yes;" + "database=Database_Mile3;" + "connection timeout=5;" + "MultipleActiveResultSets = true";
        private UserAccess userAccess;
        private LogAccess logAccess;
        public DataAccess()
        {
            userAccess = new UserAccess();
            logAccess = new LogAccess();
        }
        public void addLog(Log log)//Changes the logs.
        {
            this.logAccess.insertLog(log);
        }

        public void changeUsers(string[] users)//Changes the users.
        {
            userAccess.changeUsers(users);
        }

        public List<Log> getLogs()//Return the logs from the database.
        {
            return logAccess.getLogs();
        }

        public bool verify(string username, string password)
        {
            return userAccess.verify(username, password);
        }

        public List<User> getUsersOfRole(string role)
        {
            return userAccess.getUsersOfRole(role);
        }

        public void changePassword(User user, string newPassword)
        {
            userAccess.changePassword(user, newPassword);
        }

        public void deleteUser(User user)
        {
            userAccess.deleteUser(user);
        }

        public void addUser(User user)
        {
            userAccess.addUser(user);
        }

        public void changeRole(User user, string newRole)
        {
            userAccess.changeRole(user, newRole);
        }

        public bool isExist(string username)
        {
            return userAccess.isExist(username);
        }

        public string getRole(string username)
        {
            return userAccess.getRole(username);
        }

        public void changeEmail(User user)
        {
            throw new NotImplementedException();
        }

        public void setLiveAlert(User user, bool option)
        {
            userAccess.setLiveAlert(user, option);
        }

        public bool [] getAdminsProps(User user)
        {
            return userAccess.getAdminsProps(user);
        }
        public void setAdminsProps(bool [] props,User user)
        {
            userAccess.setAdminsProps(props,user);
        }

        public User getUser(string username)
        {
            return userAccess.getUser(username);
        }

        public bool isDataConnection()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                conn.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
