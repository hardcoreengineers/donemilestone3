﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedClasses
{
    public class Log
    {
        private string action;
        private string time;
        private string userPerformed;
        private string userAffected;
        public Log(string action, string time, string userPerformed, string userAffected)
        {
            this.action = action;
            this.time = time;
            this.userPerformed = userPerformed;
            this.userAffected = userAffected;
        }
        public string getAction()
        {
            return this.action;
        }
        public string getTime()
        {
            return this.time;
        }
        public string getUserPerformed()
        {
            return this.userPerformed;
        }
        public string getUserAffected()
        {
            return this.userAffected;
        }
        public override string ToString()
        {
            if(userAffected.Equals(""))
            {
                return this.time + ", " + this.action + ", " + this.userPerformed;
            }
            else
            {
                return this.time + ", " + this.action + ", " + this.userPerformed + ", " + this.userAffected;
            }
        }
    }
}
